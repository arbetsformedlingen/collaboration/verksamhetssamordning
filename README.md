# Verksamhetssamordning


## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)

## Verksamhetssamordning för enheten Jobtech

Det här är inte en heltäckande sammanställning utan syftar till att lyfta upp huvudprocesser så att dessa synkar med enhetens olika road-maps och milstolpar. Frågor om innehållet kan ställas till enheten-jobtech@arbetsformedlingen.se

## Sammanfattande överblick över innehållet i samordningen

- Verksamhetsplanering
- Verksamhetsuppföljning
- Ekonomisk uppföljning
- Arkitekturstyrning
- Hantering av förfrågningar till enheten

För mer information se [Wiki](https://gitlab.com/arbetsformedlingen/collaboration/verksamhetssamordning/-/wikis/Verksamhetssamordning)